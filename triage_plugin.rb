module TriagePlugin
  def mr_id
    url.match(/(?<=merge_requests\/)[0-9]*/)
  end

  def assignees
    resource[:assignees]
  end

  def url
    resource[:web_url]
  end

  def attachedLabels
    resource[:labels]
  end
end

Gitlab::Triage::Resource::Context.include TriagePlugin
