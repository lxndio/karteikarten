# Karteikarten Module TU-Dortmund
Dieses Repository enthält Anki-Decks zu diversen Modulen der TU-Dortmund.

## Was ist Anki?
Anki ist eine Lernsoftware, die wir zum Lernen für die Klausuren benutzen.
Anki basiert auf einer [Lernkartei](https://de.wikipedia.org/wiki/Lernkartei), also einem klassischen Karteikartensystem. Nur digital und viel besser!!1elf1!1!

Anki kannst du sowohl auf deinem [PC](https://apps.ankiweb.net/), als auch auf deinem [iOS](https://apps.apple.com/us/app/ankimobile-flashcards/id373493387) oder [Android](https://play.google.com/store/apps/details?id=com.ichi2.anki)-Gerät benutzen. Die Karteikarten kannst du zwischen den Geräten über [ankiweb](https://apps.ankiweb.net/) synchronisieren.

## Was ist CrowdAnki?
CrowdAnki ist ein AddOn für Anki, das es uns ermöglicht, kollaborativ Anki-Decks zu erstellen. Mit CrowdAnki kann man Anki-Decks so exportieren, dass diese in einem Git-Repository verwaltet werden können. Somit können wir die klassischen Verfahren der kollaborativen Softwareentwicklung nutzen, um unsere Karteikarten zu erstellen. Das heißt Pull Requests für Karteikarten. Geil, oder?

CrowdAnki findest du [hier](https://ankiweb.net/shared/info/1788670778)

## Wie installiere ich CrowdAnki?

1. Installiere Anki
2. Öffne Anki und gehe auf `Tools` &rightarrow; `Add-ons` &rightarrow; `Get Addon`
3. Gib die Nummer `1788670778` ein.
4. Klicke auf OK. Das Add-on sollte nun installiert sein.
5. Starte Anki neu und öffne erneut das `Add-ons`-Fenster
6. Wähle links CrowdAnki aus und öffne die Config
7. Kopiere folgendes in das Feld *Deck Sort Method(s)*: `guid, note_model_id`
8. Jetzt sollte CrowdAnki einsatzbereit sein!

## Wie importiere ich Decks?
Lade unsere Decks [hier](https://gitlab.com/dodod/karteikarten) herunter.

Zum Managen der Mediendateien nutzen wir [git-lfs](https://git-lfs.github.com/).

Ob du es installiert hast, kannst du prüfen, indem du `git lfs --version` ausführst. Solltest du es noch nicht installiert haben, kannst du es dir [hier](https://git-lfs.github.com/) herunterladen und installieren. Viele Linux-Distributionen bieten Git LFS auch in den Standardpaketquellen an.

Führe anschließend `git lfs pull` in deinem lokalen Repository aus, um die Mediendateien mit git lfs herunterzuladen.

Importiere dann das jeweilige Deck unter `File` &rightarrow;`CrowdAnki: Import from disk` und wähle den Ordner des Moduls aus, das du importieren willst.

Zu guter Letzt solltest du noch `Tools` &rightarrow; `Check Media...` ausführen, um die verwendeten Grafiken neu zu erzeugen.

## Wie synchronisiere ich meine Karteikarten auf mein Smartphone bzw. andere Geräte?
Für die Synchronisation zwischen deinen Geräten bietet Anki eine eigen Synchronisationsfunktion an. Klick einfach auf den `Sync`-Button in Anki. Anki führt dich dann durch den restlichen Prozess.

Eine genaue Anleitung findest du sonst auch [hier](https://apps.ankiweb.net/docs/am-manual.html#cloud-sync).

## Mithelfen
Eine ausführliche Erklärung, wie du uns helfen kannst, findest du
[hier](CONTRIBUTE.md)

## Aktuell gibt es für folgende Module Anki-Decks:
| Deck  |   Beschreibung 
|-------|----------------
| MafI1 | Allgemeines Deck für MafI 1
| RS    | Allgemeines Deck für Rechnerstrukturen

## Für Modul XY gibt es kein Deck D:
Da wir selber im ersten Semester sind, haben wir erstmal nur Karten für RS und MafI1 geschrieben. Zudem können wir auch nur Karten aus den Modulen überprüfen die wir selber bereits belegt haben.
Wenn du uns [helfen](CONTRIBUTE.md) willst, auch andere Module anzubieten, schreib uns!!!11!1
