## Wie kann ich helfen?
Grunsätzlich hilft es immer, wenn du zu Themen, für die noch keine Karteikarten gibt, welche schreibst und uns einen Merge-Request (siehe [hier](#wie-erstelle-ich-meinen-ersten-merge-request)) sendest.

Ebenfalls brauchen wir **immer** Personen, die neue Karten korrekturlesen und uns somit beim Bearbeiten der Merge-Requests helfen, gerade auch für Module, die wir selber (noch) nicht belegt haben.

## Wie exportiere ich die Decks?
Ein Deck kanns du exportieren unter &rightarrow;`Tools`&rightarrow;`Export`&rightarrow;`Export format: CrowdAnki JSON representation (*directory)` &rightarrow;`Include: *Wähle dein Fach aus*` &rightarrow;`[x] Include Media` &rightarrow; `[x] Include tags` &rightarrow; `Export`. 

**Wichtig:** Wähle als Ordner das Repository aus, **nicht** das Deck! CrowdAnki erstellt sonst einen neuen Ordner in dem jeweiligen Deck. Du möchtest aber das bestehende Deck aktualisieren. 

Wie du deinen Merge-Request erstellst, um deine Karten mit uns zu teilen, findest du [hier](#wie-erstelle-ich-meinen-ersten-merge-request), falls du dich mit Git noch nicht so gut auskennst.

## Wie erstelle ich meinen ersten Merge-Request?
Für die Verwaltung der Decks nutzen wir [**git**](https://git-scm.com/). Wenn du mit Git noch nie gearbeitet hast, kannst du das [hier](https://rogerdudler.github.io/git-guide/index.de.html) nachholen. Da du Informatik studierst, wirst du Git sowieso irgendwann lernen müssen (und auch wollen, Git ist nicht ohne Grund das meistverwendete SCM).

Wie man einen Merge-Request erstellt, wird z.B. [hier](https://docs.gitlab.com/ee/user/project/merge_requests/) erklärt. Nachdem du einen Merge-Request erstellt hast, überpfüfen wir deine Karten nochmal (formelle Korrektheit, Typo's etc.). Wie wir deine Merge-Request bearbeiten erklären wir [hier](#was-ist-das-crowdanki-diff-tool). Sollte es Unregelmäßigkeiten geben, kriegst du von uns eine Rückmeldung.


## Was ist das CrowdAnki-Diff-Tool?
Wir überprüfen eingehende neue Karten oder Änderungen bestehender Karten grundlegend auf Korrektheit. Dafür haben wir ein kleines Tool gebastelt, das es uns erleichtert die Anki-Karten im Browser anzuzeigen. 

![CrowdAnki-Diff-Tool](docs/AnkiDiff.png "CrowdAnki-Diff-Tool")
